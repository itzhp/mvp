package com.itzhp.simplemvp;

public interface LoginView {
    void showValidationErrorMsg();

    void loginSuccessFully();

    void loginFail();
}
