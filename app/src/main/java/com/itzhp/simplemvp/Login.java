package com.itzhp.simplemvp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Login extends AppCompatActivity implements LoginView {
    private TextView textViewUserName;
    private TextView textViewPassword;
    private Button buttonLogin;
    private Loginpresenter presenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textViewUserName = findViewById(R.id.textViewUserName);
        textViewPassword = findViewById(R.id.textViewPassword);
        buttonLogin = findViewById(R.id.buttonLogin);
        presenter = new LoginPresenterImpl(this);
        buttonLogin.setOnClickListener(v ->
                presenter.handleLogin(textViewUserName.getText().toString(), textViewPassword.getText().toString()));

    }


    @Override
    public void showValidationErrorMsg()
    {
        Toast.makeText(this, "Required", Toast.LENGTH_LONG).show();
    }
    @Override
    public void loginSuccessFully()
    {
        Toast.makeText(this, "Login SuccessFully", Toast.LENGTH_LONG).show();
    }
    @Override
    public void loginFail()
    {
        Toast.makeText(this, "Username or Password is incorrect", Toast.LENGTH_LONG).show();
    }
}