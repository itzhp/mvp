package com.itzhp.simplemvp;

public class LoginPresenterImpl implements Loginpresenter {
    private LoginView loginView;

    public LoginPresenterImpl(LoginView loginView)
    {
        this.loginView = loginView;
    }
    @Override
    public void handleLogin(String username, String password)
    {
        if (username.isEmpty()||password.isEmpty()){
            loginView.showValidationErrorMsg();
        }else {
            if (username.equals("Standerd") && password.equals("Standerd"))
            {
                loginView.loginSuccessFully();
            }
            else
            {
                loginView.loginFail();
            }
        }
    }
}